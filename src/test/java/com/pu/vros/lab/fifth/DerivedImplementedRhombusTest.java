package com.pu.vros.lab.fifth;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DerivedImplementedRhombusTest {
    private Figure rhombus;

    @Before
    public void before(){
        this.rhombus = new DerivedImplementedRhombus(10, 8);
    }

    @Test
    public void getPerimeter() {
        assertEquals( 80d, rhombus.getArea(), 0d );
    }

    @Test
    public void getArea() {
        assertEquals( 40d, rhombus.getPerimeter(), 0d );
    }

    @Test
    public void isFigureTrue() {
        assertEquals( true, rhombus.isFigure() );
    }

    @Test
    public void isFigureFalse() {
        Figure notFigure = new DerivedImplementedRhombus(0,5);
        assertEquals( false, notFigure.isFigure() );
    }

    @Test
    public void isFigureFalse1() {
        Figure notFigure = new DerivedImplementedRhombus(1,-5);
        assertEquals( false, notFigure.isFigure() );
    }

    @Test( expected = IllegalArgumentException.class)
    public void notFigureException() {
        Figure notFigure = new DerivedImplementedRhombus(1,-5);
        notFigure.getArea();
    }

    @Test( expected = IllegalArgumentException.class)
    public void notFigureException2() {
        Figure notFigure = new DerivedImplementedRhombus(1,-5);
        notFigure.getPerimeter();
    }
}