package com.pu.vros.lab.sixth;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HoursTaxiBillCalculatorTest {
    private BillCalculator billCalculator;
    private double tripAmount;
    private double waitingAmount;

    @Before
    public void before(){
        this.billCalculator = new HoursTaxiBillCalculator();
        this.tripAmount = 7;
        this.waitingAmount = 16;
    }

    @Test
    public void calculateTripBill() {
        assertEquals( 5250, billCalculator.calculateTripBill( tripAmount ), 0D );
    }

    @Test
    public void calculateWaitingBill() {
        assertEquals( 80, billCalculator.calculateWaitingBill( waitingAmount ), 0D );
    }

    @Test
    public void calculateResultBill() {
        assertEquals( 5330, billCalculator.getResultBill( tripAmount, waitingAmount ), 0D );
    }}