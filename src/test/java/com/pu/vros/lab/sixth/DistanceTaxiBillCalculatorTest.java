package com.pu.vros.lab.sixth;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DistanceTaxiBillCalculatorTest {

    private BillCalculator billCalculator;
    private double tripAmount;
    private double waitingAmount;
    @Before
    public void before(){
        this.billCalculator = new DistanceTaxiBillCalculator();
        this.tripAmount = 7;
        this.waitingAmount = 16;
    }

    @Test
    public void calculateTripBill() {
        assertEquals( 1500D, billCalculator.calculateTripBill( tripAmount ), 0D );
    }

    @Test
    public void calculateWaitingBill() {
        assertEquals( 20, billCalculator.calculateWaitingBill( waitingAmount ), 0D );
    }

    @Test
    public void calculateResultBill() {
        assertEquals( 1520, billCalculator.getResultBill( tripAmount, waitingAmount ), 0D );
    }
}