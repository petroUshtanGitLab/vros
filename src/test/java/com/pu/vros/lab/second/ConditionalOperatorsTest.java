package com.pu.vros.lab.second;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConditionalOperatorsTest{

    private ConditionalOperators conditionalOperators;

    @Before
    public void before(){
        this.conditionalOperators = new ConditionalOperators();
    }

    @Test
    public void describeNumberTest1(){
        assertEquals("від'ємне однозначне число", conditionalOperators.describeNumber( -1 ));
    }

    @Test
    public void describeNumberTest2(){
        assertEquals("від'ємне двозначне число", conditionalOperators.describeNumber( -21 ));
    }

    @Test
    public void describeNumberTest3(){
        assertEquals("від'ємне тризначне число", conditionalOperators.describeNumber( -452 ));
    }

    @Test
    public void describeNumberTest4(){
        assertEquals("нуль", conditionalOperators.describeNumber( 0 ));
    }

    @Test
    public void describeNumberTest5(){
        assertEquals("додатне однозначне число", conditionalOperators.describeNumber( 9 ));
    }

    @Test
    public void describeNumberTest6(){
        assertEquals("додатне двозначне число", conditionalOperators.describeNumber( 64 ));
    }

    @Test
    public void describeNumberTest7(){
        assertEquals("додатне тризначне число", conditionalOperators.describeNumber( 222 ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void describeNumberTestError(){
        conditionalOperators.describeNumber( -6469 );
    }

    @Test(expected = IllegalArgumentException.class)
    public void describeNumberTestError1(){
        conditionalOperators.describeNumber( 1324 );
    }

}