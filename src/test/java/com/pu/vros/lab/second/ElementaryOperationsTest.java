package com.pu.vros.lab.second;

import org.junit.Before;
import org.junit.Test;

import java.util.function.BiFunction;

import static org.junit.Assert.assertEquals;

public class ElementaryOperationsTest {

    private ElementaryOperations elementaryOperations;

    @Before
    public void before() {
        this.elementaryOperations = new ElementaryOperations();
    }

    @Test
    public void getMiddleGeometricalOfModules1() {
        doMiddleGeometricalTest(-1.5D, 2.3D, 1.857417562100671D);
    }

    @Test
    public void getMiddleGeometricalOfModules2() {
        doMiddleGeometricalTest(1.5D, -2.3D, 1.857417562100671D);
    }

    @Test
    public void getMiddleGeometricalOfModules3() {
        doMiddleGeometricalTest(0D, 2.3D, 0d);
    }

    @Test
    public void getMiddleGeometricalOfModules4() {
        doMiddleGeometricalTest(1.5D, 0D, 0D);
    }

    @Test
    public void getMiddleGeometricalOfModules5() {
        doMiddleGeometricalTest(-1.5D, -2.3D, 1.857417562100671D);
    }

    @Test
    public void getMiddleArithmeticalOfCubes1() {
        doMiddleArithmeticalTest(-1.5D, -2.3D, -7.770999999999999d);
    }

    @Test
    public void getMiddleArithmeticalOfCubes2() {
        doMiddleArithmeticalTest(1.5D, -2.3D, -4.395999999999999d);
    }

    @Test
    public void getMiddleArithmeticalOfCubes3() {
        doMiddleArithmeticalTest(-1.5D, 2.3D, 4.395999999999999d);
    }

    @Test
    public void getMiddleArithmeticalOfCubes4() {
        doMiddleArithmeticalTest(1.5D, 0D, 1.6875d);
    }

    @Test
    public void getMiddleArithmeticalOfCubes5() {
        doMiddleArithmeticalTest(-1.5D, 0D, -1.6875d);
    }

    private void doMiddleGeometricalTest(Double first, Double second, Double result) {
        doAssetion(first, second, result, elementaryOperations::getMiddleGeometricalOfModules);
    }

    private void doMiddleArithmeticalTest(Double first, Double second, Double result) {
        doAssetion(first, second, result, elementaryOperations::getMiddleArithmeticalOfCubes);
    }

    private void doAssetion(Double first, Double second, Double result, BiFunction<Double, Double, Double> action) {
        assertEquals(Double.valueOf(result), action.apply(first, second));
    }
}