package com.pu.vros.lab.second;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SorterTest {

    @Test
    public void getDescendingSortedArray1() {
        doSortingTest(
                arrayOf(1, 2, 3, 4),
                arrayOf(4, 3, 2, 1));
    }

    @Test
    public void getDescendingSortedArray2() {
        doSortingTest(
                arrayOf(2, 1, 3, 4),
                arrayOf(4, 3, 2, 1));
    }

    @Test
    public void getDescendingSortedArray3() {
        doSortingTest(
                arrayOf(3, 2, 1, 4),
                arrayOf(4, 3, 2, 1));
    }

    @Test
    public void getDescendingSortedArray4() {
        doSortingTest(
                arrayOf(1, 4, 2, 3),
                arrayOf(4, 3, 2, 1));
    }

    @Test
    public void getDescendingSortedArray5() {
        doSortingTest(
                arrayOf(4, 3, 2, 1),
                arrayOf(4, 3, 2, 1));
    }

    @Test
    public void getDescendingSortedArray6() {
        doSortingTest(
                arrayOf(4, 4, 4, 4),
                arrayOf(4, 4, 4, 4));
    }

    private void doSortingTest(int[] array, int[] manuallySortedArray) {
        Sorter sorter = new Sorter(array);
        Assert.assertArrayEquals(manuallySortedArray, sorter.getDescendingSortedArray());
    }

    private int[] arrayOf( int ... ints ){
        return ints;
    }
}