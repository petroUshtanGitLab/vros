package com.pu.vros.lab.second;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CyclesTest {

    private Cycles cycles;

    @Before
    public void before(){
        this.cycles = new Cycles();
    }

    @Test
    public void calculateSumOfRow() {
        Assert.assertEquals(Double.valueOf(-0.09467685885699312), cycles.calculateSumOfRow() );
    }

}