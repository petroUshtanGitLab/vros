package com.pu.vros.lab.third;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RhombusTest {

    private Rhombus rhombus;

    @Before
    public void before(){
        this.rhombus = new Rhombus(10, 8);
    }

    @Test
    public void getPerimeter() {
        assertEquals( 80d, rhombus.getArea(), 0d );
    }

    @Test
    public void getArea() {
        assertEquals( 40d, rhombus.getPerimeter(), 0d );
    }

    @Test
    public void isFigureTrue() {
        assertEquals( true, rhombus.isFigure() );
    }

    @Test
    public void isFigureFalse() {
        Rhombus notFigure = new Rhombus(0,5);
        assertEquals( false, notFigure.isFigure() );
    }

    @Test
    public void isFigureFalse1() {
        Rhombus notFigure = new Rhombus(1,-5);
        assertEquals( false, notFigure.isFigure() );
    }

    @Test( expected = IllegalArgumentException.class)
    public void notFigureException() {
        Rhombus notFigure = new Rhombus(1,-5);
        notFigure.getArea();
    }

    @Test( expected = IllegalArgumentException.class)
    public void notFigureException2() {
        Rhombus notFigure = new Rhombus(1,-5);
        notFigure.getPerimeter();
    }
}