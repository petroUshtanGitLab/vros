package com.pu.vros.lab.fourth;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DerivedRhombusTest {
    private Quadrangle rhombus;

    @Before
    public void before(){
        this.rhombus = new DerivedRhombus(10, 8);
    }

    @Test
    public void getPerimeter() {
        assertEquals( 80d, rhombus.getArea(), 0d );
    }

    @Test
    public void getArea() {
        assertEquals( 40d, rhombus.getPerimeter(), 0d );
    }

    @Test
    public void isFigureTrue() {
        assertEquals( true, rhombus.isFigure() );
    }

    @Test
    public void isFigureFalse() {
        Quadrangle notFigure = new DerivedRhombus(0,5);
        assertEquals( false, notFigure.isFigure() );
    }

    @Test
    public void isFigureFalse1() {
        Quadrangle notFigure = new DerivedRhombus(1,-5);
        assertEquals( false, notFigure.isFigure() );
    }

    @Test( expected = IllegalArgumentException.class)
    public void notFigureException() {
        Quadrangle notFigure = new DerivedRhombus(1,-5);
        notFigure.getArea();
    }

    @Test( expected = IllegalArgumentException.class)
    public void notFigureException2() {
        Quadrangle notFigure = new DerivedRhombus(1,-5);
        notFigure.getPerimeter();
    }
}