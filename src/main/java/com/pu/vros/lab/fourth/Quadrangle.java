package com.pu.vros.lab.fourth;

import java.util.function.Supplier;

public abstract class Quadrangle {

    abstract double getPerimeter();
    abstract double getArea();
    abstract boolean isFigure();

    protected double exseptionHandler(Supplier<Double> supplier) {
        if(!isFigure()){
            throw new IllegalArgumentException("At least one parameter has non positive value");
        }
        return supplier.get();
    }
    protected boolean isParameterValid( double parameter ){
        return parameter > 0;
    }
}
