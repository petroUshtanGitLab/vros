package com.pu.vros.lab.fourth;

public class DerivedRhombus extends Quadrangle {
    private double sideLength;
    private double height;

    public DerivedRhombus(double sideLength, double height ){
        this.height = height;
        this.sideLength = sideLength;
    }

    public double getSideLength() {
        return sideLength;
    }

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getPerimeter(){
        return exseptionHandler( () -> sideLength * 4 );
    }

    public double getArea(){
        return exseptionHandler( () -> sideLength*height );
    }

    public boolean isFigure(){
        return  isParameterValid(sideLength) && isParameterValid(height);
    }

}
