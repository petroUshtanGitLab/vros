package com.pu.vros.lab.fifth;

import java.util.function.Supplier;

public abstract class Quadrangle implements Figure {

    protected double exseptionHandler(Supplier<Double> supplier) {
        if(!isFigure()){
            throw new IllegalArgumentException("At least one parameter has non positive value");
        }
        return supplier.get();
    }
    protected boolean isParameterValid( double parameter ){
        return parameter > 0;
    }
}
