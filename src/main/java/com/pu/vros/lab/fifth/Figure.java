package com.pu.vros.lab.fifth;

public interface Figure {
    double getPerimeter();
    double getArea();
    boolean isFigure();
}
