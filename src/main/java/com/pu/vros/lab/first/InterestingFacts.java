package com.pu.vros.lab.first;

public class InterestingFacts {
    private static final String firstName = "Petro";
    private static final String lastName = "Ushtan";
    private static final char firstNameCharacter = 'P';
    private static final int age = 21;
    private static final short height = 169;
    private static final byte weight = 72;
    private static final long year = 5l;
    private static final float desiredScore = 4.9f;
    private static final double distanceToHome = 89.951d;
    private static final boolean isMarried = false;

    private static String addLine( String key, String value ){
        return key + " = " + value + "\n";
    }

    private static String buildText(){
        return "Interesting facts about me:\n"
                + addLine( "First name", firstName )
                + addLine( "Last name", lastName)
                + addLine( "First letter of my name", String.valueOf(firstNameCharacter))
                + addLine( "Age",String.valueOf(age))
                + addLine( "Height", String.valueOf(height))
                + addLine( "Weight", String.valueOf(weight))
                + addLine( "Year", String.valueOf(year))
                + addLine( "Desired score", String.valueOf(desiredScore))
                + addLine( "Distance to home", String.valueOf(distanceToHome))
                + addLine( "Is married", String.valueOf(isMarried) );
    }

    public static void main(String[] args) {
        System.out.println(buildText());
    }
}
