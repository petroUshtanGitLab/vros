package com.pu.vros.lab.third;

public class Rhombus {
    private double sideLength;
    private double height;

    public Rhombus( double sideLength, double height ){
        this.height = height;
        this.sideLength = sideLength;
    }

    public double getSideLength() {
        return sideLength;
    }

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getPerimeter(){
        validateFigure();
        return sideLength * 4;
    }

    private void validateFigure() {
        if(!isFigure()){
            throw new IllegalArgumentException("At least one parameter has non positive value");
        }
    }

    public double getArea(){
        validateFigure();
        return sideLength*height;
    }

    public boolean isFigure(){
        return sideLength > 0 && height > 0;
    }

}
