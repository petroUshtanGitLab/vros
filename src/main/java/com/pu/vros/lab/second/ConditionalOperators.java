package com.pu.vros.lab.second;

// variant 8
public class ConditionalOperators {

    public String describeNumber(int number){
        if (number < -999 || number > 999) {
            throw new IllegalArgumentException("Number is out of range: " + number);
        }

        if( number == 0 ){
            return "нуль";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(checkSign(number));
        stringBuilder.append(checkDigitNumber(number));
        stringBuilder.append("значне число");
        return stringBuilder.toString();
    }

    private String checkSign( int number ){
        return number > 0 ? "додатне " : "від'ємне ";
    }
    private String checkDigitNumber( int number ){
        int digitNum = String.valueOf(Math.abs(number)).length();
        switch( digitNum ){
            case 1: return "одно";
            case 2: return "дво";
            case 3: return "три";
            default:
                throw new IllegalArgumentException("Too many digits:" + digitNum);
        }
    }
}
