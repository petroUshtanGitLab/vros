package com.pu.vros.lab.second;

//variant 8
public class Cycles {

    public Double calculateSumOfRow() {
        double previousSum = 0;
        double currentSum = 0;
        long power = 0;
        do{
            previousSum = currentSum;
            currentSum += calculateValueAtOneIteration(power);
            power++;
        }while( Math.abs(currentSum - previousSum) > 0.00000000000000001 );
        return currentSum;
    }

    private Double calculateValueAtOneIteration(long power) {
        return (Math.cos(Math.pow(2, power) + Math.pow(3, power))) / (calculateFactorial(power*power));
    }

    long calculateFactorial(long n) {
        long result = 1;
        for (long i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

}
