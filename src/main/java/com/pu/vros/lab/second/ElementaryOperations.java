package com.pu.vros.lab.second;

// variant 8
public class ElementaryOperations {

    public Double getMiddleGeometricalOfModules(Double first, Double second ){
        return getMiddleGeometrical( getAbs(first), getAbs(second) );
    }

    public Double getMiddleArithmeticalOfCubes(Double first, Double second) {
        return getMiddle(getCube(first), getCube(second));
    }

    private Double getCube(Double x) {
        return Math.pow(x, 3);
    }

    private Double getMiddle(Double x, Double y) {
        return (x + y) / 2;
    }

    private Double getAbs(Double x ){
        return Math.abs(x);
    }

    private Double getMiddleGeometrical(Double x, Double y){
        if( x<0 || y<0 ){
            throw new IllegalArgumentException("x= "+ x + " and y " + y + "should be integral");
        }
        return Math.sqrt( x*y );
    }

}
