package com.pu.vros.lab.second;

public class Sorter {
    private int[] array;

    public Sorter(int[] array) {
        this.array = array;
    }

    public int[] getDescendingSortedArray() {
        int[] sortedArray = this.array.clone();
        int temp;
        for (int i = 1; i < sortedArray.length; i++) {
            for (int j = i; j > 0; j--) {
                if (sortedArray[j] > sortedArray[j - 1]) {
                    temp = sortedArray[j];
                    sortedArray[j] = sortedArray[j - 1];
                    sortedArray[j - 1] = temp;
                }
            }
        }
        return sortedArray;
    }
}
