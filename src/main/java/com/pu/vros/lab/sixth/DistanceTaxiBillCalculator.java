package com.pu.vros.lab.sixth;

public class DistanceTaxiBillCalculator implements BillCalculator{

    @Override
    public double calculateTripBill(double kilometers) {
        return kilometers > 2 ? 30 * 10 * ( kilometers-2 ) : 30;
    }

    @Override
    public double calculateWaitingBill(double minutes) {
        return minutes * 1.25;
    }

}
