package com.pu.vros.lab.sixth;

public class HoursTaxiBillCalculator implements BillCalculator {

    @Override
    public double calculateTripBill(double hours) {
        return hours * 750;
    }

    @Override
    public double calculateWaitingBill(double minutes) {
        return minutes * 5;
    }
}
