package com.pu.vros.lab.sixth;

public interface BillCalculator {
    double calculateTripBill( double amount );
    double calculateWaitingBill( double amount );
    default double getResultBill( double tripAmount, double waitingAmount ){
        return calculateTripBill( tripAmount ) + calculateWaitingBill( waitingAmount );
    }
}
