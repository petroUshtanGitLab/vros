package com.pu.vros.lab.seventh;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class WordAppearingCalculator {

    public static void main(String[] args) throws IOException {
        String text = readFile( ClassLoader.getSystemClassLoader().getResource("hp2.txt").getPath(),
                Charset.defaultCharset() );
        String[] words = text.split("[, \n]");
        Set<String> uniqueWords = new HashSet<>();

        for (String word : words) {
            uniqueWords.add(word);
        }

        uniqueWords.forEach(System.out::println);

        uniqueWords.forEach(
                uniqueWord -> {
                    System.out.println(
                            uniqueWord + " = "+ Arrays.stream(words).filter(word-> uniqueWord.equals(word) ).count()
                    );
                }
        );
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
